package main

import (
"net/http"
"encoding/json"
"fmt"
)


//http://steamcommunity.com/id/<CUSTOMURL>/inventory/json/753/6 
//http://steamcommunity.com/profiles/<PROFILEID>/inventory/json/753/6
//http://stackoverflow.com/questions/17393099/getting-someones-steam-inventory

func getAPIKey() string {
	return "736277AD997E38AE27A14389E798D8C0"
}
func getAPIKeyParameter() string{
	return "&key=" + getAPIKey()
}

func getSteamID() string{
	return "76561197970489794"
}

func getSteamIDParameter() string{
	return "&steamid=" + getSteamID()
}

func main() {

	client := http.Client{}

	friendIDs:= getFriendIDs(client)

	for _, element := range friendIDs{
		fmt.Println(element)
	}

	playerSummaries := getPlayerSummaries(client,friendIDs)
fmt.Println(len(playerSummaries))
	for i := range playerSummaries{
		fmt.Println(playerSummaries[i].Personaname + " (" + playerSummaries[i].Realname + ")")
	}
}


func getFriendIDs(client http.Client) []string {
	url := "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?relationship=friend" + getAPIKeyParameter() + getSteamIDParameter()
	resp,_ := client.Get(url)
	response := new(FriendListResponse)
	json.NewDecoder(resp.Body).Decode(&response)

	ids := make([]string,len(response.FriendsList.Friends))

	for index , element := range response.FriendsList.Friends {
		ids[index] = element.Steamid
	}

	return ids
}

type PlayerSummariesResponse struct
{
	Response struct {
		Players []PlayerSummary
	}

}
type PlayerSummary struct{
	Steamid string
	Communityvisibilitystate uint
	Profilestate uint
	Personaname string
	Lastlogoff uint
	Profileurl string
	Avatar string
	Avatarmedium string
	Avatarfull string
	Personastate uint
	Realname string
	Primaryclanid string
	Timecreated uint
	Personastateflags uint
	Loccountrycode string
	Locstatecode string
	Loccityid uint
}

type FriendListResponse struct{
	FriendsList struct{
		Friends []Friend
	}
}

type Friend struct{
	Steamid string
	Relationship string
	Friend_since uint
}

func getPlayerSummaries(client http.Client, steamids []string ) []PlayerSummary {

	steamidsParameter := "steamids="
	for i := range steamids{
		steamidsParameter += steamids[i] +","
	}


	url := "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?" + steamidsParameter + getAPIKeyParameter() 

	resp,_ := client.Get(url)
	response := new(PlayerSummariesResponse)
	json.NewDecoder(resp.Body).Decode(&response)

	return response.Response.Players
}


type Game struct{
	Appid uint
	Name string
	Playtime_forever uint
	Img_icon_url string
	Img_logo_url string
	Has_community_visible_stats bool
}

type OwnedGamesResponse struct{
	Response struct{
		Game_count uint
		Games []Game
	}
}

func getOwnedGames(client http.Client, appinfo bool, freegames bool) Game[]{

}